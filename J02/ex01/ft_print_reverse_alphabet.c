#include <unistd.h>
#include "lire.h"

void ft_print_reverse_alphabet(void)
{
	char l = 'z';
	while (l >= 'a')
	{
		ft_putchar(l);
		l--;
	}
}
