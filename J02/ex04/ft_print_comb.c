#include<unistd.h>
#include "lire.h"

void ft_print_def(char d, char e, char f)
{

	ft_putchar(d + 48);
	ft_putchar(e + 48);
	ft_putchar(f + 48);
	if (d != 7)
	{	
		ft_putchar(',');
		ft_putchar(' ');
	}
}	

void ft_print_comb(void)
{
	char d;
	char e;
	char f;

	d = 0;
	while (d <= 7)
	{
		e = 1;
		while (e <= 8)
		{
			f = 2;
			while (f <= 9)
			{
				if ((d != e && d != f && e != f) && ( d < e && e < f))
					ft_print_def(d, e, f);
				f++;
			}
			e++;
		}
		d++;
	}
	ft_putchar('\n');

}	
