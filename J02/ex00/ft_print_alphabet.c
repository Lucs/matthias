#include <unistd.h>
#include <lire.h>

void ft_print_alphabet(void);
{
	char l = 'a';
	while (l <= 'z')
	{
		ft_putchar(l);
		l = l + 1;
	}
}	
